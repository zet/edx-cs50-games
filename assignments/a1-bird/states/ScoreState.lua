--[[
    ScoreState Class
    Author: Colton Ogden
    cogden@cs50.harvard.edu

    A simple state used to display the player's score before they
    transition back into the play state. Transitioned to from the
    PlayState when they collide with a Pipe.
]]

-- since we only want the image loaded once, not per instantation, define it externally
local IMAGE_GOLD = love.graphics.newImage('star_gold.png')
local IMAGE_SILVER = love.graphics.newImage('star_silver.png')
local IMAGE_BRONZE = love.graphics.newImage('star_bronze.png')

SCORE_GOLD = 12
SCORE_SILVER = 7
SCORE_BRONZE = 3

ScoreState = Class{__includes = BaseState}

--[[
    When we enter the score state, we expect to receive the score
    from the play state so we know what to render to the State.
]]
function ScoreState:enter(params)
    self.score = params.score
    self.award = nil
end

function ScoreState:update(dt)
    -- define if there's an award
    if self.score >= SCORE_GOLD then
        self.award = IMAGE_GOLD
    elseif self.score >= SCORE_SILVER then
        self.award = IMAGE_SILVER
    elseif self.score >= SCORE_BRONZE then
        self.award = IMAGE_BRONZE
    else
        self.award = nil
    end

    -- go back to play if enter is pressed
    if love.keyboard.wasPressed('enter') or love.keyboard.wasPressed('return') then
        gStateMachine:change('countdown')
    end
end

function ScoreState:render()
    -- simply render the score to the middle of the screen
    love.graphics.setFont(flappyFont)
    love.graphics.printf('Oof! You lost!', 0, 64, VIRTUAL_WIDTH, 'center')

    love.graphics.setFont(mediumFont)
    love.graphics.printf('Score: ' .. tostring(self.score), 0, 100, VIRTUAL_WIDTH, 'center')

    if self.award then
        love.graphics.draw(self.award, VIRTUAL_WIDTH / 2 - 16, 120)
    end

    love.graphics.printf('Press Enter to Play Again!', 0, 160, VIRTUAL_WIDTH, 'center')
end