# CS50's Introduction to Game Development

https://courses.edx.org/courses/course-v1:HarvardX+CS50G+Games/course/

## My Assignments

_Note, I use [love v11.1](https://love2d.org), installed via `brew cask install love`. Isn't it beautiful? 😊_

0. 🈸 [Pong](./assignments/a0-pong/main.lua)         ← 📝 [The AI Update](https://docs.cs50.net/games/2018/g/assignments/0/assignment0.html)
1. 🈸 [Flappy Bird](./assignments/a1-bird/main.lua)  ← 📝 [The Reward Update](https://docs.cs50.net/games/2018/g/assignments/1/assignment1.html)